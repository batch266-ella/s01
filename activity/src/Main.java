import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        double average;

        Scanner userInput = new Scanner(System.in);

        System.out.println("Enter your First Name: ");
        firstName = userInput.nextLine();

        System.out.println("Enter your Last Name: ");
        lastName = userInput.nextLine();

        System.out.println("Enter your First Subject Grade: ");
        firstSubject = new Double(userInput.nextLine());

        System.out.println("Enter your Second Subject Grade: ");
        secondSubject = new Double(userInput.nextLine());

        System.out.println("Enter your Third Subject Grade: ");
        thirdSubject = new Double(userInput.nextLine());

        average = (firstSubject + secondSubject + thirdSubject) / 3;
        System.out.println("Good day, " + firstName + " " + lastName + ". Your grade average is: " + average);
    }
}