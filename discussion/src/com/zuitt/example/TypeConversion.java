package com.zuitt.example;

import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
//        System.out.println("2" + 2);
//        System.out.println("How old are you? ");
//        Scanner age = new Scanner(System.in);
//        String userAge = age.nextLine();
//        System.out.println("The age is " + userAge);
        System.out.println("How old are you? ");
        Scanner userInput = new Scanner(System.in);
        double age = new Double(userInput.nextLine());

        System.out.println("This is a confirmation that you are " + age + " years old");
    }
}
